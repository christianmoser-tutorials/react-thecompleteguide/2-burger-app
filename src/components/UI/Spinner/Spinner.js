import React from 'react'

// CSS from https://projects.lukehaas.me/css-loaders/
import classes from './Spinner.module.css'

function Spinner(props) {
  return (
    <div className={classes.Loader}>Loading...</div>
  )
}

Spinner.propTypes = {

}

export default Spinner

